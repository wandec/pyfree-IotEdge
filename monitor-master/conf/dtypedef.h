#ifndef _DATA_TYPE_DEF_H_
#define _DATA_TYPE_DEF_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : dtypedef.h
  *File Mark       : 
  *Summary         : 自定义数据类型
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
namespace pyfree{
//设备配置
enum DevType
{
	VirtaulDev = 1, //虚拟设备
	EntityDev = 3,  //实体设备
	DevDef = 0      //未定义
};
//信息点类型
enum PType
{
	OnYX = 1,//遥信
	OnYC = 2,//遥测
	OnYXS = 3,//组合遥信
	OnYCS = 4,//组合遥测
	PTDef = 0
};

//计划配置
enum PlanType
{
	OnTime = 1,			//定时
	OnPoll = 2,			//轮询
	OnCondition = 3,	//条件
	OnStart = 4,		//启动
	PlanTDef = 0
};
//执行策略
enum ExeType
{
	OnGet = 1,		//查询
	OnSet = 2,		//设值
	ExeTDef = 0
};
//数值校对类型
enum CompareType
{
	OnEqual = 1,
	OnMore = 2,
	OnLess = 3,
	ComTDef = 0
};

//事件类型
enum EventType
{
	YXChg = 1,		//遥信变位
	YXShake = 2,		//遥信抖动
	YCUpOverLimit = 3,		//遥测越上限
	YCDownOverLimit = 4,		//遥测越下限
	YCBreak = 5,		//遥测跳变
	YCDisturb = 6,		//遥测扰动
	YKOpFail = 7,		//遥控失败
	YTOpFail = 8,		//遥调失败
	YXNoChange = 9,		//遥信长期不变
	YCNoChange = 10,		//遥测长期不变
	YXNoUpdate = 11,		//遥信长期不更新
	YCNoUpdate = 12,		//遥测长期不更新
	UserDefCondition = 13,		//用户定义条件告警
	UserDefRestart = 14,		//用户定义重启告警
	UserDefRout = 15,		//用户定义轮询告警
	UserDefTime = 16,		//用户定义定时告警
	DefAlarmType = 0
};
//事件等级
enum EventLevel
{
	normalLevel = 1,	//一般
	majorLevel = 2,	//主要
	seriousLevel = 3,	//严重
	DefLevel = 0
};
//告警方式
enum EventWay
{
	AlarmForLog = 1,		//日志
	AlarmForSMS = 2,		//短信
	AlarmForEMail = 3,		//邮件
	AlarmForDef = 0
};
}

#endif
