#ifndef _CONF_PLAN_H_
#define _CONF_PLAN_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_plan.h
  *File Mark       : 
  *Summary         : 任务计划配置
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <vector>
#include <string>

#include "dtypedef.h"

namespace pyfree
{

struct ExceptTime
{
	int myear;	//年
	int mmon;	//月
	int mday;	//日
};
//定时计划使用
struct PlanTime
{
	int myear;	//年
	int mmon;	//月
	int mday;	//日
	int mhour;	//时
	int mmin;	//分
	int wday;	//星期
	bool flag;	//标识
	std::vector<ExceptTime>	exceptList;
};
//定期计划
struct PlanSleep
{
	unsigned int interval;
	unsigned int tt;
	//unsigned int startHour;
	//unsigned int endHour;
};

struct PlanProperty
{
	int			planID;
	PlanType	planType;
	std::string desc;
	int			planEffect;
	unsigned long planSuccessFlag;	//任务成功执行标记挂钟时间,单位毫秒
	unsigned long planInterval;		//任务成功执行后需要间隔一段时间才继续巡检,单位毫秒
};


//条件策略的条件集
struct PlanCondition
{
	int			cID;
	long long	devID;
	DevType		devType;
	unsigned int pID;
	PType		pType;
	float		val;
	CompareType compare;
	bool        ValChange;//变化是否有效
};
//条件策略的总条件
//struct ConditionProperty
//{
//	unsigned int startHour;
//	unsigned int startMin;
//	unsigned int endHour;
//	unsigned int endMin;
//
//	unsigned int allStartMin;
//	unsigned int allEndMin;
//};
//条件策略的总条件
struct TVProperty
{
	unsigned int startHour;
	unsigned int startMin;
	unsigned int endHour;
	unsigned int endMin;

	unsigned int allStartMin;
	unsigned int allEndMin;
};
//开机任务
struct PlanForStart
{
	PlanForStart() : waitSec(10), execFlag(false)
	{
	};
	unsigned int waitSec;
	bool execFlag;
};

struct PlanCMD
{
	long long	devID;
	DevType		devType;
	unsigned int pID;
	PType		pType;
	ExeType		exeType;
	float		val;
	int			waitT;
	int			timedelay;
	std::string desc;
	//depend 
	long long		dDevID;
	unsigned int	dPID;
	unsigned int	vtime;		//下控验证等待时间,单位秒,默认值及最小值为5
};

struct Plan
{
	PlanProperty			planAtt;
	std::vector<PlanTime>	ptimes;
	PlanSleep				psleep;
	//ConditionProperty		conAtt;
	PlanForStart			pFStart;
	std::vector<TVProperty> timeVS;
	std::vector<PlanCondition> plancons;
	std::vector<PlanCMD>	planCmds;
};
}

#endif
