#include "socket_local.h"

#include "socket_private_acl.h"
#include "socket_srv_acl.h"
#include "socket_local_read.h"
#include "socket_local_write.h"
#include "Log.h"
////////////////////////////////////////////////*SocketLocal*///////////////////////////////////////////////////////////////

SocketLocal::SocketLocal(std::string ip,unsigned int port)
{
	socket_acl = new SocketPrivate_ACL(port,ip);
	if(socket_acl->onConnect()>0)
	{
		socket_srv_acl = new SocketSrv_ACL();
		socket_srv_acl->setPDataPtr(socket_acl);
		socket_srv_acl->start();

		socket_l_read = new SocketLocalRead();
		socket_l_read->setPrivateDataPtr(socket_acl);
		socket_l_read->start();

		socket_l_write = new SocketLocalWrite();
		socket_l_write->setPrivateDataPtr(socket_acl);
		socket_l_write->start();

	}else{
		socket_srv_acl=NULL;
		socket_l_read=NULL;
		socket_l_write = NULL;
		CLogger::createInstance()->Log(MsgError,
			"listen port(%d) error, [%s %s %d]", port
			, __FILE__, __FUNCTION__, __LINE__);
	}
}

SocketLocal::~SocketLocal(void)
{
	if (NULL != socket_l_read) 
	{
		delete socket_l_read;
		socket_l_read = NULL;
	}
	if (NULL != socket_l_write) 
	{
		delete socket_l_write;
		socket_l_write = NULL;
	}
	if (NULL != socket_srv_acl) 
	{
		delete socket_srv_acl;
		socket_srv_acl = NULL;
	}
	if(NULL!=socket_acl)
	{
		delete socket_acl;
		socket_acl = NULL;
	}
}

//int SocketLocal::reSetSocket()
//{
//	if (NULL != socket_acl) {
//		return socket_acl->reSetSocket();
//	}
//	else {
//		return -1;
//	}
//}
//
//int SocketLocal::Read(char* buf, int size)
//{
//	if (NULL != socket_acl) {
//		return socket_acl->Read(buf, size);
//	}
//	else {
//		return -1;
//	}
//}

int SocketLocal::Write(const char* buf, int size)
{
	if (NULL != socket_acl) 
	{
		return socket_acl->Write(buf, size);
	}
	else {
		return -1;
	}
}
