#include "verifyforcontrol.h"

#include "verifyforcontrolcache.h"

#ifdef WIN32
#define usleep(x) Sleep(x)
#endif

VerifyForControl::VerifyForControl(void) 
	: running(true)
	, ptr_VCC(VerifyForControlCache::getInstance())
{

};

VerifyForControl::~VerifyForControl(void)
{
	running = false;
};

void* VerifyForControl::run()
{
	while (running)
	{
		ptr_VCC->checkVerifyData();
		usleep(10);
	}
	return NULL;
}
