#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _IO_MQTT_H_
#define _IO_MQTT_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : io_mqtt.h
  *File Mark       :
  *Summary         : mqtt通信接口线程
  *基于第三方mqtt库(Mosquitto)开发的通信接口示例
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#ifdef linux
#include <string>
#endif

class PyMQTTIO;
class ProducerMQTT;
class ConsumerMQTT;

class IOMQTT : public acl::thread
{
public:
	IOMQTT(void);
	virtual ~IOMQTT(void);

	void* run();
private:
	/**
	 * 连接mqtt服务
	 * @return {void}
	 */
	void connect();
	/**
	 * 断开mqtt服务
	 * @return {void}
	 */
	void disconnect();
	/**
	 * 删除mqtt接口实例,在本接口退出时调用
	 * @return {void}
	 */
	void cleanup();
private:
	bool running;			//线程运行标志
	PyMQTTIO *mqttcpp;		//mqtt通信接口实现
	ProducerMQTT *producer; //mqtt的生产者接口
	ConsumerMQTT *consumer; //mqtt的消费者接口
	std::string ip_s;		//mqtt服务端地址
	int port_i;				//mqtt服务端侦听端口
	std::string user_s;		//mqtt用户名
	std::string password_s; //mqtt密码
	bool logf;				//mqtt连接或断连日志记录标记,确保只有通信连接态势变化时才做日志记录
};

#endif
