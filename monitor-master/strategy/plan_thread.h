#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#ifndef _PLAN_THREAD_H_
#define _PLAN_THREAD_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : plan_thread.h
  *File Mark       : 
  *Summary         : 
  *1)任务巡检线程,根据设定策略进行业务逻辑驱动
  *2)任务计划分为定时 定期 条件 启动等类型
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include <vector>

#include "conf_plan.h"
#include "datadef.h"
#include "queuedata_single.h"
#include "strategy.h"

class VerifyForControlCache;
class DelayPlanThread;

class PlanThread : public acl::thread,public Strategy
{
public:
	PlanThread(void);
	~PlanThread(void);
	void* run();

	void setDelayPlanThread(DelayPlanThread *ptr_);
protected:
private:
	void doCmd(std::vector<PlanCMD> planCmds);
	void addDelayExec(DataToGather wd);
private:
	bool running;
	bool is_Verification;
	VerifyForControlCache *ptr_vcc;
	DelayPlanThread *ptr_delay_plan;
	//
	unsigned int timePlan;
	std::vector<pyfree::Plan> plans;
	//
	QueueDataSingle<DataToGather> *socket_write_queue;
};
#endif
