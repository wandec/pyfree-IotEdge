#include "alarm_thread.h"

#ifdef WIN32
#define usleep(x) Sleep(x)
#endif

#include <time.h>
#include "pfunc.h"
#include "Log.h"

#include "business_def.h"

AlarmThread::AlarmThread(void)
	: Strategy()
	, running(true)
	, queueforwar(QueueDataSingle<EventForWaring>::getInstance())
	, timePlan(static_cast<unsigned int>(time(NULL)))
{
	plans = pbdef->getAlarms();
};

AlarmThread::~AlarmThread(void)
{
	running = false;
};

void* AlarmThread::run()
{
	unsigned int planSize = static_cast<unsigned int>(plans.size());
	unsigned int curTimeSec = 0;
	while (running)
	{
		for(unsigned int i=0; i<planSize; ++i)
		{
			//执行间隔条件,执行成功后需间隔一段时间才能再次进行条件巡检
			if (plans[i].planAtt.planInterval>0
				&&(plans[i].planAtt.planSuccessFlag+ plans[i].planAtt.planInterval)>static_cast<unsigned long>(pyfree::getClockTime())) 
			{
				continue;
			}
			curTimeSec = static_cast<unsigned int>(time(NULL));
			switch (plans[i].planAtt.planType)
			{
			case 1:
			{
				if (mapCurTimes(plans[i].ptimes)
					&& (timePlan + plans[i].planAtt.planEffect)<curTimeSec)
				{
					doCmd(plans[i].alarmCmds, plans[i].planAtt.planID);
					if(plans[i].planAtt.planInterval>0)
						plans[i].planAtt.planSuccessFlag = static_cast<unsigned long>(pyfree::getClockTime());
				}
			}
				break;
			case 2:
			{
				if (mapSleepTime(plans[i].timeVS,plans[i].psleep)
					&& (timePlan + plans[i].planAtt.planEffect)<curTimeSec)
				{
					doCmd(plans[i].alarmCmds, plans[i].planAtt.planID);
					if (plans[i].planAtt.planInterval>0)
						plans[i].planAtt.planSuccessFlag = static_cast<unsigned long>(pyfree::getClockTime());
				}
			}
				break;
			case 3:
			{
				if (conditionJude(plans[i].timeVS,plans[i].plancons,false)
					&& (timePlan + plans[i].planAtt.planEffect)<curTimeSec)
				{
					doCmd(plans[i].alarmCmds, plans[i].planAtt.planID);
					if (plans[i].planAtt.planInterval>0)
						plans[i].planAtt.planSuccessFlag = static_cast<unsigned long>(pyfree::getClockTime());
					//printf("conditionJude success!\n");
				}
			}
				break;
			case 4:
				if (!plans[i].pFStart.execFlag
					&& (timePlan + plans[i].pFStart.waitSec)<curTimeSec
					&& (timePlan + plans[i].pFStart.waitSec+60)>curTimeSec
					&& mapStartTime(plans[i].timeVS))
				{
					plans[i].pFStart.execFlag = true;
					doCmd(plans[i].alarmCmds, plans[i].planAtt.planID);
					if (plans[i].planAtt.planInterval>0)
						plans[i].planAtt.planSuccessFlag = static_cast<unsigned long>(pyfree::getClockTime());
				}
				break;
			default:
				break;
			}
		}
		usleep(10);
	}
	return 0;
};

void AlarmThread::doCmd(std::vector<AlarmCMD> planCmds, int planID)
{
	int day_ = getCurTVDay();
	for (std::vector<AlarmCMD>::iterator it = planCmds.begin();
		it != planCmds.end(); ++it)
	{
		//日期变更重置次数
		if (it->dayFlag != day_)
		{
			it->dayFlag = day_;
			it->dayAlarmCount = 0;
		}
		//告警次数每天有限制
		if (it->dayAlarmCount >= it->dayAlarmCountLimit)
		{
			continue;
		}
		//告警间隔不能太频繁
		int curTime_ = static_cast<int>(time(NULL));
		if ((curTime_ - it->alarmTime) < it->alarmInterval)
		{
			continue;
		}
		it->alarmTime = curTime_;
		//告警描述
		EventForWaring event_;
		event_.type		= it->type;
		event_.grade	= it->level;
		event_.send_	= it->way;
		event_.execTime = pyfree::getCurrentTimeByFormat("%04d%02d%02dT%02d%02d%02dZ");
		event_.taskID	= static_cast<unsigned long>(1000* planID+it->ID);
		event_.taskDesc = "UserDefAlarm";
		event_.devID	= it->devID;
		event_.devDesc	= it->devDesc;
		event_.pID		= it->pID;
		event_.pDesc	= it->pDesc;
		if (it->desc.length() <= 19) 
		{
			event_.valDesc = it->desc;
		}
		else 
		{
			event_.valDesc = it->desc.substr(0, 19);
		}
		//全状况描述
		char buf_comment[256] = { 0 };
		if (it->devID > 0 && it->pID > 0) 
		{
			sprintf(buf_comment
				, "alarm for user define,desc(%s),devID(%ld),devDesc(%s),pID(%ld),pDesc(%s)"
				, it->desc.c_str()
				, it->devID, it->devDesc.c_str()
				, it->pID, it->pDesc.c_str());
		}
		else if (it->devID > 0) 
		{
			sprintf(buf_comment
				, "alarm for user define,desc(%s),devID(%ld),devDesc(%s)"
				, it->desc.c_str()
				, it->devID, it->devDesc.c_str());
		}
		else 
		{
			sprintf(buf_comment
				, "alarm for user define,desc(%s)"
				, it->desc.c_str());
		}
		event_.Comment = std::string(buf_comment);
		//CLogger::createInstance()->Log(eTipMessage, "%s", buf_comment);
		queueforwar->add(event_);
		it->dayAlarmCount += 1;
	}
};

