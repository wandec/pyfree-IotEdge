#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_GATHER_READ_H_
#define _SOCKET_GATHER_READ_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socket_gather_read.h
  *File Mark       :
  *Summary         : 
  *该线程从各个客户端(采控服务/中控服务)读取数据,并将数据分帧,并将数据写入缓存队列
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "datadef.h"
#include "queuedata_single.h"

class SocketPrivate_ACL;

class SocketGatherRead : public acl::thread
{
public:
	SocketGatherRead(void);
	~SocketGatherRead(void);
  /**
	 * 设置通信接口指针
	 * @param socket_acl_ {SocketPrivate_ACL* } 通信接口,基于acl库实现
	 * @return {void}
	 */
	void setPrivateDataPtr(SocketPrivate_ACL *socket_acl_);
	void* run();
private:
	bool running;     //线程运行标记
	SocketPrivate_ACL *socket_acl; //通信接口
	//本线程只负责数据读取和拼帧,将数据写入队列,由额外线程进行数据解析和中转
	QueueDataSingle<DataFromChannel> *socket_cache_queue;
};

#endif