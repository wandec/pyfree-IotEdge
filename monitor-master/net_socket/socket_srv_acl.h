#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_SRV_ACL_H_
#define _SOCKET_SRV_ACL_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socket_srv_acl.h
  *File Mark       : 
  *Summary         : 
  *基于acl第三方库开发的TCP-Socket的服务监听线程,监听来自采集服务的连接
  *服务记录每个连接的具体信息,在数据读取和写入时具有针对性处理
  *当前数据读和写线程分开,采集端推送的数据在读线程获取后,推送带缓存队列,再由独立的线程处理具体业务
  *后续考虑到业务处理的复杂性,该独立线程会在更改为线程池进行业务处理
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

class SocketPrivate_ACL;

class SocketSrv_ACL : public acl::thread
{
public:
	SocketSrv_ACL();
	~SocketSrv_ACL();

  /**
	 * 设置通信接口指针
	 * @param socket_acl_ {SocketPrivate_ACL* } 通信接口,基于acl库实现
	 * @return {void}
	 */
	void setPDataPtr(SocketPrivate_ACL *socket_acl_);
	void* run();
private:
	bool running;       //线程运行标记
	SocketPrivate_ACL *socket_acl; //通信接口
};
#endif
