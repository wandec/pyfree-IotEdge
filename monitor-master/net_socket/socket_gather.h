#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_GATHER_H_
#define _SOCKET_GATHER_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socket_gather.h
  *File Mark       :
  *Summary         : 
  *作为采集服务上层通信的的服务端接口,并为数据读写以及数据帧解析建立单独处理的线程
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>

class SocketPrivate_ACL;
class SocketSrv_ACL;
class SocketGatherRead;
class SocketGatherWrite;
class SocketCacheThread;

class SocketGather
{
public:
	SocketGather(std::string ip,unsigned int port);
	virtual ~SocketGather(void);
public:

	virtual int Read(){ return -1; };
	virtual int Write(){ return -1; };
//protected:
	//int Read(char* buf, int size);
	int Write(const char* buf, int size);
private:
	SocketPrivate_ACL *socket_acl;    //tcp-socket通信接口
	SocketSrv_ACL *socket_srv_acl;    //通信端口监听线程
	SocketGatherRead *socket_g_read;  //数据读取线程
	SocketGatherWrite *socket_g_write;//数据写入线程
	SocketCacheThread *socket_cache_thread;//缓存数据处置线程
};

#endif //_MYSOCKET_H_
