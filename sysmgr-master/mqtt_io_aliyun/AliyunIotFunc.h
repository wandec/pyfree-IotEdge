#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _ALIYUN_IOT_FUNC_H_
#define _ALIYUN_IOT_FUNC_H_
/***********************************************************************
  *Copyright 2020-04-12, pyfree
  *
  *File Name       : AliyunIoFunc.h.h
  *File Mark       : 
  *Summary         : 阿里云物联网平台功能函数集,主要回调函数实现
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "iot_import.h"
#include "iot_export.h"

#define  PAYLOAD_FORMAT				"{\"id\":\"%lld\",\"version\":\"1.0\",\"method\":\"%s\",\"params\":%s}"
#define  ALINK_METHOD_PROP_POST		"thing.event.property.post"
#define  ALINK_METHOD_EVENT_POST	"thing.event.ControlFail.post"
#define  ALINK_METHOD_EVENT_UP		"thing.event.%s.post"

#define ALINK_COMMENT_FORMAT				"clientId%s&%sdeviceName%sproductKey%stimestamp%lld"
#define ALINK_TOPIC_DEV_LOGIN				"/ext/session/%s/%s/combine/login"
#define ALINK_TOPIC_DEV_LOGIN_REPLY			"/ext/session/%s/%s/combine/login_reply"
#define ALINK_TOPIC_EVENT_PRO_POST			"/sys/%s/%s/thing/event/property/post"
#define ALINK_TOPIC_EVENT_PRO_POST_REPLY	 "/sys/%s/%s/thing/event/property/post_reply"
#define ALINK_TOPIC_SERVICE_PRO_SET          "/sys/%s/%s/thing/service/property/set"

//config get
#define ALINK_TOPIC_CONFIG_GET				"/sys/%s/%s/thing/config/get"
#define ALINK_TOPIC_CONFIG_GET_REPLY		"/sys/%s/%s/thing/config/get_reply"
//config push
#define ALINK_TOPIC_CONFIG_PUSH				"/sys/%s/%s/thing/config/push"
#define ALINK_TOPIC_CONFIG_PUSH_REPLY		"/sys/%s/%s/thing/config/push_reply"
//client up event
#define ALINK_TOPIC_EVENT_UP				"/sys/%s/%s/thing/event/%s/post"
#define ALINK_TOPIC_EVENT_UP_REPLY			"/sys/%s/%s/thing/event/%s/post_reply"
//server down event
#define ALINK_TOPIC_EVENT_DOWN				"/sys/%s/%s/thing/service/%s"
#define ALINK_TOPIC_EVENT_DOWN_REPLY		"/sys/%s/%s/thing/service/%s_reply"

#define login_fomat		\
"{\"id\":\"%lld\",\"params\":{ \"productKey\":\"%s\",\"deviceName\":\"%s\",\"clientId\":\"%s&%s\",\"timestamp\":\"%lld\",\"signMethod\":\"hmacSha1\",\"sign\":\"%s\",\"cleanSession\":\"true\"}}"
#define add_fomat		\
"{\"id\":\"%lld\",\"version\":\"1.0\",\"params\":[{\"deviceName\":\"%s\",\"productKey\":\"%s\",\"sign\":\"%s\",\"signmethod\":\"hmacSha1\",\"timestamp\":\"%lld\",\"clientId\":\"%d\"}],\"method\":\"thing.topo.add\"}"
#define MQTT_MSGLEN             (2048)

#define  OTA_BUF_LEN			(4096)

#define EXAMPLE_TRACE(fmt, ...)  \
    do { \
        HAL_Printf("%s|%03d :: ", __func__, __LINE__); \
        HAL_Printf(fmt, ##__VA_ARGS__); \
        HAL_Printf("%s", "\r\n"); \
    } while(0)
//下面为阿里云物联网接口相关的回调函数集
void event_handle(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
void login_message_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
void push_reply_message_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
void service_set_message_arrive(void *pcontext, void *pclient, iotx_mqtt_event_msg_pt msg);
//主题信息打印输出
void printf_out_topic_info(iotx_mqtt_topic_info_pt ptopic_info);
#endif
