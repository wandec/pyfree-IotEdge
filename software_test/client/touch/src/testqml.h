#include <QObject>
#include <QVariant>
#include <QDebug>

class PCSClient;

class MyClass : public QObject
{
	Q_OBJECT
public:
    MyClass();
signals:
    void addDev(QVariant devID,QVariant devType,QVariant name, QVariant desc);
    void addPInfo(QVariant devID,QVariant pID,QVariant name,QVariant desc,QVariant pType,QVariant val);
    void PValue(QVariant devID,QVariant pID,QVariant dtime, QVariant val);
public slots:
    void setPValue(int devID,int pID,qreal val);
private slots:

private:
    PCSClient *ptr_PC;
};
