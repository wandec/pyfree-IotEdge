#include "plothistogram.h"

#include "qwt/qwt_painter.h"

MyQwtPlotHistogram::MyQwtPlotHistogram(const QString &title)
    : QwtPlotHistogram(title)
{

}

MyQwtPlotHistogram::MyQwtPlotHistogram(const QwtText &title)
    : QwtPlotHistogram(title)
{
    init();
}

MyQwtPlotHistogram::~MyQwtPlotHistogram()
{

}

void MyQwtPlotHistogram::init()
{
    m_Color = NULL;
}

void MyQwtPlotHistogram::setMyColorData(QVector< QColor >* color)
{
    m_Color = color;
}

/*!
  Draw a subset of the histogram samples

  \param painter Painter
  \param xMap Maps x-values into pixel coordinates.
  \param yMap Maps y-values into pixel coordinates.
  \param canvasRect Contents rect of the canvas
  \param from Index of the first sample to be painted
  \param to Index of the last sample to be painted. If to < 0 the
         series will be painted to its last sample.

  \sa drawOutline(), drawLines(), drawColumns
*/
void MyQwtPlotHistogram::drawSeries( QPainter *painter,
    const QwtScaleMap &xMap, const QwtScaleMap &yMap,
    const QRectF &, int from, int to ) const
{
    if ( !painter || dataSize() <= 0 )
        return;

    if ( to < 0 )
        to = dataSize() - 1;

    switch ( this->style() )
    {
        case QwtPlotHistogram::Outline:
            drawOutline( painter, xMap, yMap, from, to );
            break;
        case QwtPlotHistogram::Lines:
            drawLines( painter, xMap, yMap, from, to );
            break;
        case QwtPlotHistogram::Columns:
            drawColumns( painter, xMap, yMap, from, to );
            break;
        case QwtPlotHistogram::UserStyle:
            drawMyColumns(painter, xMap, yMap, from, to);
            break;
        default:
            drawColumns( painter, xMap, yMap, from, to );
            break;
    }
}

/*!
  Draw a histogram in Columns style()

  \param painter Painter
  \param xMap Maps x-values into pixel coordinates.
  \param yMap Maps y-values into pixel coordinates.
  \param from Index of the first sample to be painted
  \param to Index of the last sample to be painted. If to < 0 the
         histogram will be painted to its last point.

  \sa setStyle(), style(), setSymbol(), drawColumn()
*/
void MyQwtPlotHistogram::drawMyColumns( QPainter *painter,
    const QwtScaleMap &xMap, const QwtScaleMap &yMap,
    int from, int to ) const
{
//    painter->setPen( this->pen() );
//    painter->setBrush( this->brush() );

    const QwtSeriesData<QwtIntervalSample> *series = data();

    for ( int i = from; i <= to; i++ )
    {
        painter->setPen( QPen(m_Color->value(i)) );
        painter->setBrush( QBrush(m_Color->value(i)) );
        const QwtIntervalSample sample = series->sample( i );
        if ( !sample.interval.isNull() )
        {
            const QwtColumnRect rect = columnRect( sample, xMap, yMap );
            drawColumn( painter, rect, sample );
        }
    }
}

/*!
  Calculate the area that is covered by a sample

  \param sample Sample
  \param xMap Maps x-values into pixel coordinates.
  \param yMap Maps y-values into pixel coordinates.

  \return Rectangle, that is covered by a sample
*/
QwtColumnRect MyQwtPlotHistogram::columnRect( const QwtIntervalSample &sample,
    const QwtScaleMap &xMap, const QwtScaleMap &yMap ) const
{
    QwtColumnRect rect;

    const QwtInterval &iv = sample.interval;
    if ( !iv.isValid() )
        return rect;

    if ( orientation() == Qt::Horizontal )
    {
        const double x0 = xMap.transform( baseline() );
        const double x  = xMap.transform( sample.value );
        const double y1 = yMap.transform( iv.minValue() );
        const double y2 = yMap.transform( iv.maxValue() );

        rect.hInterval.setInterval( x0, x );
        rect.vInterval.setInterval( y1, y2, iv.borderFlags() );
        rect.direction = ( x < x0 ) ? QwtColumnRect::RightToLeft :
                         QwtColumnRect::LeftToRight;
    }
    else
    {
        const double x1 = xMap.transform( iv.minValue() );
        const double x2 = xMap.transform( iv.maxValue() );
        const double y0 = yMap.transform( baseline() );
        const double y = yMap.transform( sample.value );

        rect.hInterval.setInterval( x1, x2, iv.borderFlags() );
        rect.vInterval.setInterval( y0, y );
        rect.direction = ( y < y0 ) ? QwtColumnRect::BottomToTop :
            QwtColumnRect::TopToBottom;
    }

    return rect;
}

/*!
  Draw a column for a sample in Columns style().

  When a symbol() has been set the symbol is used otherwise the
  column is displayed as plain rectangle using pen() and brush().

  \param painter Painter
  \param rect Rectangle where to paint the column in paint device coordinates
  \param sample Sample to be displayed

  \note In applications, where different intervals need to be displayed
        in a different way ( f.e different colors or even using differnt symbols)
        it is recommended to overload drawColumn().
*/
void MyQwtPlotHistogram::drawColumn( QPainter *painter,
    const QwtColumnRect &rect, const QwtIntervalSample &sample ) const
{
    Q_UNUSED( sample );

    if ( this->symbol() &&
        ( this->symbol()->style() != QwtColumnSymbol::NoStyle ) )
    {
        this->symbol()->draw( painter, rect );
    }
    else
    {
        QRectF r = rect.toRect();
        if ( QwtPainter::roundingAlignment( painter ) )
        {
            r.setLeft( qRound( r.left() ) );
            r.setRight( qRound( r.right() ) );
            r.setTop( qRound( r.top() ) );
            r.setBottom( qRound( r.bottom() ) );
        }

        QwtPainter::drawRect( painter, r );
    }
}

void MyQwtPlotHistogram::dataChanged()
{
	QwtPlotHistogram::dataChanged();
}

size_t MyQwtPlotHistogram::dataSize() const
{
	return QwtPlotHistogram::dataSize();
}
QRectF MyQwtPlotHistogram::dataRect() const
{
	return QwtPlotHistogram::dataRect();
}

void MyQwtPlotHistogram::setRectOfInterest( const QRectF &rect )
{
	QwtPlotHistogram::setRectOfInterest(rect);
}