/***********************************************************************
  *Copyright (c) 2011 Easy Communication Electtrical Power Tech.Co.Ltd
  *
  *File Name       : dataitem.h
  *File Mark       :
  *Summary         : the perturbation data about the specific info from server
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

  *problem         : the data unit in a DataItem_instance must be unified,
  *                  even like A and MA isn't to consider unified,so I suggest
  *                  the data_unit attribute is describeed in DataItem,not in rowData.
 ************************************************************************/
#ifndef DATAITEM_H
#define DATAITEM_H

#include <QList>
#include <QDate>
#include <QTime>
#include <QDateTime>

class rowData;

class DataItem
{
public:
    DataItem();
    ~DataItem();

    DataItem& operator=(const DataItem& rhs);

    void append(rowData value);
    void append(QDateTime dt, qreal d, unsigned char _quality);
    void append(QDate ds,QTime t, qreal d, unsigned char _quality);
    void append(QDate ds,QTime t, qreal d, unsigned char _quality,QString unit);
    void prepend(rowData value);
    void prepend(QDate ds,QTime t, qreal d, unsigned char _quality);
    void prepend(QDate ds,QTime t, qreal d, unsigned char _quality,QString unit);

    QList<rowData> getData();
    QTime getMinQTime();
    QTime getMaxQTime();
    qreal getMinData();
    qreal getMaxData();
    rowData value(int i);
    bool isEmpty();
    int size();
    rowData getLast();
    void removeLast();

    //2014-03-04
    void setName(QString _n);
    QString getName();

    QString getDesc();
private:
    void init();
private:
    QString name;
    QList<rowData> data;
};

////////////////////////////////////////////////////////

class rowData
{
public:
    rowData();
    explicit rowData(QDate dt,QTime t, qreal d, unsigned char _quality,QString unit = QString("second"));
    rowData(const rowData& rValue);
    rowData& operator=(const rowData& rValue);
    ~rowData();

    QDateTime getDateTime();
	  QDate getDate();
    QTime getTime();
    qreal getData();
    unsigned char getQDS();
    QString getDescribe();
    QString getDataUnit();
private:
    void changeDesc();
private:
  	QDate date;
    QTime time;
    qreal data;
    unsigned char quality;
    QString describe;
    QString dataUnit;
};

#endif // DATAITEM_H
