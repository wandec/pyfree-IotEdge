#ifndef PFUNC_QT_H
#define PFUNC_QT_H

#include <QString>
#include <QDateTime>
#include <QDate>
#include <QTime>

namespace PFUNC_QT
{
	int getChlId(QString _chlname);
    unsigned int getChlUId(QString _chlname);
    unsigned int getPUId(QString _pname);
	QString getDateTimeFormat();
	QString getDateFormat();
	QString getTimeFormat();

	QString ChangeDateTime(const QDateTime m_QDateTime);
    QDateTime ChangeStr(const QString m_DateTimeStr);

    QString ChangeDateTimeS(const QDateTime m_QDateTime);
    QDateTime ChangeStrS(const QString m_DateTimeStr);

    QDate ChangeDateStr(const QString m_DateStr);
    QString ChangeDate(const QDate m_QDate);

    QTime ChangeTimeStr(const QString m_QTimeStr);
    QString ChangeTime(const QTime m_QTime);

    QDateTime ChangeLong(const long long m_DateLong);
    long long ChangeDateTimeL(const QDateTime m_QDateTime);

    QDateTime ChangeLongLong(const long long m_DateLong);
    long long ChangeDateTimeLL(const QDateTime m_QDateTime);
};

#endif //PFUNC_QT_H
