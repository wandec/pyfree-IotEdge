#ifndef PLOTDATA_H
#define PLOTDATA_H

#include <QtCore/qglobal.h>

static int qwtVerifyRange( int size, int &i1, int &i2 )
{
    if ( size < 1 )
        return 0;

    i1 = qBound( 0, i1, size - 1 );
    i2 = qBound( 0, i2, size - 1 );

    if ( i1 > i2 )
        qSwap( i1, i2 );

    return ( i2 - i1 + 1 );
}

////////////////////////////////////
/*
  * define a Interval
*/
class MyInterval{
public:
    MyInterval(double vl, double minv, double maxv);
    ~MyInterval();

    double getvalue();
    double getmin();
    double getmax();
private:
    double  	value;
    double  	min;
    double  	max;
};
///////////////////////////////
/*
  *define a color
*/
class MyColor{
public:
    MyColor(int rv, int gv, int bv, int av = 255);
    ~MyColor();

    int red();
    int green();
    int blue();
    int apha();
private:
    void init();
    int changeColorValue(int vl);
private:
    int r;
    int g;
    int b;
    int a;
};
/////////////////////////////////////////
/*
  define MySetSample
  */

#include <vector>

class MySetSample
{
public:
    MySetSample(std::vector<double> setVal, double val, double xWitchVal);
    ~MySetSample();

    std::vector<double> getSet();
    double getValue();
    double getWitch();
private:
   std::vector<double>  set;
   double 	value;
   double   xWitch;
};


class CurveInterval
{
public:
    CurveInterval();
    CurveInterval(int fVal, int tVal);

    ~CurveInterval();

    int getFrom() const;
    int getTo() const;

//    void setFrom(int fVal){ from=fVal; };
//    void setTo(int tVal){ to=tVal; };
private:
    int from;
    int to;
};

#endif //PLOTDATA_H
