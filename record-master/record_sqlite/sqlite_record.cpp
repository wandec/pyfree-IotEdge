#include "sqlite_record.h"

#include <time.h>
#include <list>
#include <map>

#include "dbtypedef.h"
#include "sqlite_io.h"
#include "record_queue.h"
#include "business_def.h"
#include "File.h"
#ifdef WIN32
#define usleep(x) Sleep(x)
#endif

SqliteRecord::SqliteRecord()
	: running(true)
	, sio_ptr(NULL)
{
	acl_lib_init();
	BusinessDef *ptr_bdef = BusinessDef::getInstance();
	//记录目录
	dbDir = ptr_bdef->getGDataDir();
	m_MonRecord_b = ptr_bdef->getGMonFalg();
	sio_ptr = new SqliteIO();
	recordF = static_cast<int>(time(NULL))+10;
	ptr_rq = RecordQueue::getInstance();
};

SqliteRecord::~SqliteRecord()
{
	running = false;
    try{
        if (sio_ptr !=NULL)
        {
            if (sio_ptr->IsOpen())
            {
				sio_ptr->close();
            }
            delete sio_ptr;
			sio_ptr =NULL;
        }
    }catch(...){
		printf("SqliteRecord::destroy fail\n");
    }
};

std::string SqliteRecord::getCurDT_Str(long devID)
{
	time_t _dt = time(NULL);
	struct tm _dt_;
	char bufT[64] = { 0 };
	switch (m_MonRecord_b)
	{
	case 1://按日
#ifdef WIN32
		localtime_s(&_dt_, &_dt);
		if (0!=_dt_.tm_wday) {
			_dt -= 86400* _dt_.tm_wday;
			localtime_s(&_dt_, &_dt);
		}
		sprintf_s(bufT, "%ld\\%04d-%02d-%02d"
			, devID, _dt_.tm_year + 1900, _dt_.tm_mon + 1, _dt_.tm_mday);
#else
		localtime_r(&_dt, &_dt_);
		if (0 != _dt_.tm_wday) {
			_dt -= 86400 * _dt_.tm_wday;
			localtime_r(&_dt, &_dt_);
		}
		sprintf_s(bufT, "%ld/%04d-%02d-%02d"
			, devID, _dt_.tm_year + 1900, _dt_.tm_mon + 1, _dt_.tm_mday);
#endif
		break;
	case 2://按月
#ifdef WIN32
		localtime_s(&_dt_, &_dt);
		sprintf_s(bufT, "%ld\\%04d-%02d-01"
			, devID, _dt_.tm_year + 1900, _dt_.tm_mon + 1);
#else
		localtime_r(&_dt, &_dt_);
		sprintf_s(bufT, "%ld/%04d-%02d-01"
			, devID, _dt_.tm_year + 1900, _dt_.tm_mon + 1);
#endif
		break;
	default:
#ifdef WIN32
		localtime_s(&_dt_, &_dt);
		sprintf_s(bufT, "%ld\\%04d-%02d-%02d"
			, devID, _dt_.tm_year + 1900, _dt_.tm_mon + 1, _dt_.tm_mday);
#else
		localtime_r(&_dt, &_dt_);
		sprintf_s(bufT, "%ld/%04d-%02d-%02d"
			, devID, _dt_.tm_year + 1900, _dt_.tm_mon + 1, _dt_.tm_mday);
#endif
		break;
	}
	return std::string(bufT);
}

void* SqliteRecord::run()
{
	printf("SqliteRecord::start \n");
	std::string _curPath = "";
	time_t _curT = time(NULL);
	// int _totalSize = 0;
	// int _freeSize = 0;
	while(running){
		_curT = time(NULL);
		if (recordF < _curT)
		{
			recordF = static_cast<int>(_curT) + 30;
			//10秒记录一次
			std::map<long, std::list<SADataItem> > _wds = ptr_rq->getDatas();
			std::map<long, std::list<SADataItem> >::iterator it = _wds.begin();
			while (it != _wds.end())
			{
				if (!it->second.empty())
				{
					mkDirForDev(it->first);
					_curPath = getCurDT_Str(it->first);
					openDB(_curPath);
					if (sio_ptr->IsOpen())
					{
						sio_ptr->AddYARecorder(it->second);
						sio_ptr->close();
					}
				}
				it++;
			}
		}
		usleep(1000);
	}
	return 0;
};

void SqliteRecord::mkDirForDev(long devID)
{
	char buf[128] = { 0 };
#ifdef WIN32
	sprintf_s(buf, "%s\\%ld", dbDir.c_str(), devID);
#else
	sprintf_s(buf, "%s/%ld", dbDir.c_str(), devID);
#endif
	std::string _dir = std::string(buf);
	pyfree::createDir(_dir);
}

bool SqliteRecord::openDB(std::string  _path)
{
	std::string _db = dbDir;
#ifdef WIN32
	_db+="\\";
#else
	_db+="/";
#endif
	_db+= _path;
	_db+=".db";
	if (sio_ptr->IsOpen())
	{
		sio_ptr->close();
	}
	if (!sio_ptr->Open((char*)_db.c_str()))
	{
		printf("cann't open db:%s\n", _db.c_str());
		return false;
	}
	// else {
	// 	printf("open db:%s\n", _db.c_str());
	// }
    return true;
};
