#include "business_def.h"

#include <stdlib.h>
#include <iostream>

#include "pfunc.h"
#include "Log.h"

namespace GlobalVar {
	std::string m_app_conf_file  = "appconf.xml";
};
/////////////////////////////////////////////////////////
BusinessDef* BusinessDef::instance = NULL;
BusinessDef* BusinessDef::getInstance()
{
    if(NULL == BusinessDef::instance)
    {
        BusinessDef::instance = new BusinessDef();
    }
    return BusinessDef::instance;
}

BusinessDef::BusinessDef()
	: AppConfData()
{
	init();
};
void BusinessDef::Destroy()
{
	if(NULL!=BusinessDef::instance)
	{
		delete BusinessDef::instance;
		BusinessDef::instance = NULL;
	}
}

BusinessDef::~BusinessDef()
{
	this->Destroy();
}

void BusinessDef::init()
{
	pyfree::readAppConf(appConf,GlobalVar::m_app_conf_file);
	CLogger::createInstance()->Log(MsgInfo, "load %s finish!", GlobalVar::m_app_conf_file.c_str());
};
