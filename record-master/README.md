# pyfree-record-master

#### 介绍
用于实现历史数据实时记录,暂时支持sqlite文本数据库记录，与调度服务（pyfree-monitor-master）配套使用

#### 软件说明  

##### 架构概述 
* 1)总概述:  
    数据源来自调度软软件,暂采用udp方式通信,按需选择存储接口(如sqlite\mysql\redis等,暂实现sqlite)进行实时数据存储,存储数据可以用于历史查看、反演、分析等  
    ![组件及模块](/res/for_doc/record_com.PNG)  

* 2)类图:   
    ![记录软件主要类](/res/for_doc/record_class.PNG)   
    
##### 功能概述  

* 1)数据记录:  
    目前仅实现sqlite文本数据库记录每个信息点的历史数据曲线。每个设备建立一个存储目录，该设备的数据信息按天或按月存储到一个数据库文件中  
    提供配套的sqlite文件数据库可视化终端展示历史数据  
 

#### 编译
##### 一、依赖
1.  acl_master  
acl_master是一个跨平台c/c++库，提供了网络通信库及服务器编程框架，同时提供更多的实用功能库及示例，具体编译与实现请参考其说明文档。  
项目地址: https://gitee.com/acl-dev/acl 或 https://github.com/acl-dev/acl  
技术博客：https://www.iteye.com/blog/user/zsxxsz  

做了两处源码调整:  
由于acl提供的日志记录的时间只到秒级别，本项目需要毫秒的级别，因此修改了一下其源码，  
在lib_acl/src/stdlib/acl_mylog.c文件中，将acl_logtime_fmt函数实现调整为：  
```
void acl_logtime_fmt(char *buf, size_t size)  
{  
	//time_t	now;  
	struct timeval tm0;  
	gettimeofday(&tm0, NULL);  
	time_t	now = tm0.tv_sec;  
#ifdef	ACL_UNIX  
	struct tm local_time;  
  
	//(void) time (&now);  
	(void) localtime_r(&now, &local_time);  
	strftime(buf, size, "%Y/%m/%d %H:%M:%S", &local_time);  
	sprintf(buf, "%s.%03d ", buf,(int)(tm0.tv_usec/1000));  
#elif	defined(ACL_WINDOWS)  
	struct tm *local_time;  
  
	//(void) time (&now);  
	local_time = localtime(&now);  
	strftime(buf, size, "%Y/%m/%d %H:%M:%S", local_time);  
	sprintf(buf, "%s.%03d ",buf, (int)(tm0.tv_usec/1000));  
#else  
# error "unknown OS type"  
#endif  
}   
```
如果不需要毫秒级的时间格式，就不必修改。  

其二:  
需要修改acl库的源码,在lib_acl/src/stdlib/iostuff/acl_readable.c的int acl_readable(ACL_SOCKET fd)函数中
```
fds.events = POLLIN | POLLPRI;
```
由于win中调用了WSAPoll函数，而Winsock provider不支持POLLPRI与POLLWRBAND,调整为:
```
fds.events = POLLIN | POLLPRI;
#ifdef ACL_WINDOWS
fds.events &=~(POLLPRI|POLLWRBAND);
#endif
```

2.  sqlite  
sqlite为sqlite数据库的c接口，源码本项目经过整理为cmake工程，直接进入到sqlite目录按cmake编译即可  


##### 二、项目编译：

> linux编译
项目编译需要cmake+gcc支持，目前本项目的编译环境是centos7.3，由于需要将git相关信息写入程序,需要安装git  
或在CMakeLists.txt中注销:"execute_process(COMMAND /bin/bash ../build.sh)"  
编译命令类似如下:  
```
cd record-master 
mkdir build-linux 
cd build-linux 
cmake .. 
make
```

> win编译
当前win编译测试采用vs2015+cmake编译,例子如下  
备注:若需要32为支持请去掉Win64指定,msbuild为vs的命令编译工具,可以直接vs打开工程文件编译    
打开vs的命令行工具  
```
cd record-master  
mkdir build-win  
cd build-win  

cmake -G "Visual Studio 14 2015 Win64" -DCMAKE_BUILD_TYPE=Release ..  
msbuild pyfree-record.sln /p:Configuration="Release" /p:Platform="x64"
或
cmake -G "Visual Studio 14 2015 Win64" -DCMAKE_BUILD_TYPE=Debug ..  
msbuild pyfree-record.sln /p:Configuration="Debug" /p:Platform="x64"
```
由于acl第三方库win静态库有点大,本项目debug版本库不上传编译好的库,请自行编译,需要做一处调整
需要修改acl库的源码,在lib_acl/src/stdlib/iostuff/acl_readable.c的int acl_readable(ACL_SOCKET fd)函数中
```
fds.events = POLLIN | POLLPRI;
```
由于win中调用了WSAPoll函数，而Winsock provider不支持POLLPRI与POLLWRBAND,调整为:
```
fds.events = POLLIN | POLLPRI;
#ifdef ACL_WINDOWS
fds.events &=~(POLLPRI|POLLWRBAND);
#endif
```
#### demo示例

> linux  
1. 环境搭建  
主机win64系统  
在demo-project\historyCurve_test提供了历史数据查看终端,采用qt开发。   
如果需要linux或android版本的请去顶级目录下software_test\historyCurve自行编译  
虚拟机  
采用VMware工具创建虚拟机,centos7.3系统,本样例的网络地址设置192.168.174.130,  
开启虚拟机的共享文件夹功能，将项目挂载到虚拟机上  
  
2.  程序配置  
编译的调度软件输出在demo-project/record  
程序启动有根据磁盘或网卡校验的License约束,与采集软件一致  
因此需要向生成License的sn.txt输出文件，启动虚拟机，进入其目录执行指令构建：  
./SWL 0 22 或 ./SWL 1 22  
生成License的sn.txt，如果与采集软件部署在同一台设备，生成一次就能拷贝过来使用  
如果没用SWL程序，去顶级目录下的swLicense项目编译生成,支持cmake编译  
  
3.  项目配置  
appconf.xml：主要设置程序需要的连接信息、对外接口信息配置以及设备配置文件、调度策略文件、告警策略文件等路径指定  

> win64  
1.  程序配置  
在demo-project\historyCurve_test提供了历史数据查看终端    
编译的记录软件输出在demo-project/record  
程序启动有根据磁盘或网卡校验的License约束,与采集、调度、管理等软件一致  
因此需要向生成License的sn.txt输出文件,进入其目录执行指令构建：  
SWL.exe 0 22 或 SWL,exe 1 22  
生成License的sn.txt，如果与采集软件部署在同一台设备，生成一次就能拷贝过来使用  
如果没用SWL程序，去顶级目录下的swLicense项目编译生成,支持cmake编译  
  
服务安装,管理员启动cmd,进入demo-project/record目录：  
安装:pyfree-record.exe install  
卸载:pyfree-record.exe uninstall  
在任务管理器或服务管理页面可以启停服务或配置其服务相关信息  
更新:在任务管理器停止服务,拷贝pyfree-record.exe覆盖完成更新  

2.  项目配置  
appconf.xml：主要设置程序需要的日志存储、数据存储等信息        

