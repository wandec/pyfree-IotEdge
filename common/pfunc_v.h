#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _PFUNC_V_H_
#define _PFUNC_V_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : pfunc_v.h
  *File Mark       : 
  *Summary         : 通用函数集2
  *1)获取uuid函数
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>

namespace pyfree
{
  /**
   * 获取uid,win下采用guid,Linux下采用libuuid
   * @return {string } uuid字符串
   */
  std::string getUUID();
};

#endif
