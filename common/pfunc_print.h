#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _PFUNC_PRINT_H_
#define _PFUNC_PRINT_H_
/***********************************************************************
  *Copyright 2020-05-06, pyfree
  *
  *File Name       : pfunc_print.h
  *File Mark       : 
  *Summary         : 打印输出通用宏定义
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
typedef  enum PrintLevel
{  
	LL_NOTICE 	= 1, 	//一般输出
	LL_WARNING 	= 2, 	//告警输出
	LL_TRACE 	= 3,	//追踪调试
	LL_DEBUG 	= 4,	//软件bug
	LL_FATAL 	= 5		//致命错误
}PrintLevel;

#define Print_NOTICE(log_fmt,...) \
	do{ \
		printf("L(%d)[%s:%d][%s] \n"log_fmt"\n", LL_NOTICE,__FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); \
	}while (0) 

#define Print_WARN(log_fmt,...) \
	do{ \
		printf("L(%d)[%s:%d][%s] \n"log_fmt"\n", LL_WARNING, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); \
	}while (0) 

#define Print_TRACE(log_fmt,...) \
	do{ \
		printf("L(%d)[%s:%d][%s] \n"log_fmt"\n", LL_TRACE,__FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); \
	}while (0) 

#define Print_DEBUG(log_fmt,...) \
	do{ \
		printf("L(%d)[%s:%d][%s] \n"log_fmt"\n", LL_DEBUG, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); \
	}while (0) 

#define Print_FATAL(log_fmt,...) \
	do{ \
		printf("L(%d)[%s:%d][%s] \n"log_fmt"\n",LL_FATAL, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); \
	}while (0) 


#endif
