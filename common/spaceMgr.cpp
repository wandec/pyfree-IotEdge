#include "spaceMgr.h"
#ifdef WIN32
#define usleep(x) Sleep(x)
#endif
#include "DiskSpace.h"

DiskSpaceMgr::DiskSpaceMgr(char disk_, int fsl_,std::string dir_/*="log"*/,std::string ext_/*="log"*/)
	: DiskStr(disk_),freeSizeLimit(fsl_)
{
	DelInfo del_(dir_,ext_);
	dirs.push_back(del_);
}

DiskSpaceMgr::~DiskSpaceMgr(void)
{

}

void* DiskSpaceMgr::run()
{
	int _totalSize = 0;
	int _freeSize = 0;
	while (true)
	{
		if (pyfree::getDiskFreeSpace(DiskStr, _totalSize, _freeSize) > 0)
		{
			if (freeSizeLimit > _freeSize)
			{
				for(unsigned int i=0; i<dirs.size(); ++i)
				{
					std::string ext = dirs.at(i).ext;
					pyfree::moveOldFile(dirs.at(i).dir, ext);
				}
			}
		}
		sleep(10);
	}
	return NULL;
}

void DiskSpaceMgr::add(std::string dir_,std::string ext_)
{
	DelInfo del_(dir_,ext_);
	dirs.push_back(del_);
}
