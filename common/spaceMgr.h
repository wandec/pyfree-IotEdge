#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SPACE_MGR_H_
#define _SPACE_MGR_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : spaceMgr.h
  *File Mark       : 
  *Summary         : 磁盘空间巡检线程类，根据预留空间要求删除app日志文件
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#ifdef linux
#include <string>
#endif
#include <vector>

#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

class DiskSpaceMgr : public acl::thread
{
public:
	/**
	 * 构造函数,如传入参数D 2000 log log,则指明当D盘空闲空间不足2000M时,删除当前目录/log下的log(后缀)文件
	 * @param disk_ {char} 指定盘符
	 * @param fsl_ {int} 预留空间大小,单位MB
	 * @param dir_ {string} 指定目录
	 * @param ext_ {string} 扩展名
	 * @return { } 
	 */
	DiskSpaceMgr(char disk_, int fsl_,std::string dir_="log",std::string ext_="log");
	~DiskSpaceMgr(void);
	void* run();

	void add(std::string dir_,std::string ext_);
private:
	struct DelInfo
	{
		DelInfo(std::string dir_,std::string ext_)
			: dir(dir_), ext(ext_)
		{};
		std::string dir;
		std::string ext;
	};
private:
	//用于日志删除
	std::vector<DelInfo> dirs;  //存储目录
	char DiskStr;         //日志存储磁盘
	int freeSizeLimit;    //磁盘预留空间要求
};

#endif
