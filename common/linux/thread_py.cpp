#include "thread_py.h"

#include <stdio.h>

void* Thread_py::run0(void* pVoid)
{
    Thread_py* p = (Thread_py*) pVoid;
    p->run1();
    return p;
}

void* Thread_py::run1()
{
    threadStatus = THREAD_STATUS_RUNNING;
    tid = pthread_self();
    run();
    threadStatus = THREAD_STATUS_EXIT;
    tid = 0;
    pthread_exit(NULL);
}

Thread_py::Thread_py()
{
    tid = 0;
    threadStatus = THREAD_STATUS_NEW;
}

Thread_py::~Thread_py()
{
	join(10);
}

int Thread_py::run()
{
    while(true){
        printf("thread is running!\n");
        sleep(100);
    }
    return 0;
}

bool Thread_py::start()
{
    return pthread_create(&tid, NULL, run0, this) == 0;
}

pthread_t Thread_py::getThreadID()
{
    return tid;
}

int Thread_py::getState()
{
    return threadStatus;
}

void Thread_py::join()
{
    if (tid > 0)
    {
        pthread_join(tid, NULL);
    }
}

void Thread_py::join(unsigned long millisTime)
{
    if (tid == 0)
    {
        return;
    }
    if (millisTime == 0)
    {
        join();
    }else
    {
        unsigned long k = 0;
        while (threadStatus != THREAD_STATUS_EXIT && k <= millisTime)
        {
            usleep(100);
            k++;
        }
    }
}
 
