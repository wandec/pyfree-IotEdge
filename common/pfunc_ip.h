#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _PFUNC_IP_H_
#define _PFUNC_IP_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : pfunc_ip.h
  *File Mark       : 
  *Summary         : 网络地址相关的函数集
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>

namespace pyfree
{
/**
	 * 字符串表述的ip信息格式校验
	 * @param ip_str {string } ip地址字符串
	 * @return {bool} 格式是否正确
	 */
	bool ipCheck(std::string ip_str);
	/**
	 * 字符串表述的ip信息转换为long表述
	 * @param ip_str {string } ip地址字符串
	 * @return {long} long表述的ip信息
	 */
	long ipToInt(std::string ip_str);
	/**
	 * long表述的信息转换字符串表述的ip信息
	 * @param ip_int {long} } ip地址信息
	 * @return {string} 字符串表述的ip信息
	 */
	std::string intToIp(long ip_int);
};

#endif
