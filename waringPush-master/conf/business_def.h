#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _BUSINESS_DEF_H_
#define _BUSINESS_DEF_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_read.h
  *File Mark       : 
  *Summary         : 
  *业务数据集的前置声明,属于单体类,相当于业务数据相关的全局变量+全局函数集
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "conf_read.h"
#include "conf_app.h"

class BusinessDef
{
public:
	static BusinessDef* getInstance();
	static void Destroy();
	~BusinessDef();
public:
//电子邮件配置
	inline bool getMailWaringFunc()
	{
		return appConf.mailwar.mailWaringFunc;
	};
	inline pyfree::MailWaring getEmailConf()
	{
		return appConf.mailwar;
	};
	inline std::string getGLogDir()
	{
		return appConf.saconf.gLogDir;
	};
	
	inline char getDiskSymbol()
	{
		return appConf.saconf.diskSymbol;
	};
	inline int getFreeSizeLimit()
	{
		return appConf.saconf.freeSizeLimit;
	};
	inline int getDayForLimit()
	{
		return appConf.saconf.dayForLimit;
	};
	//
	inline std::string getLocalIp()
	{
		return appConf.saconf.local_ip;
	};
	inline int getLocalPort()
	{
		return appConf.saconf.local_port;
	};
	//msg
	inline bool getMsgWaringFunc()
	{
		return appConf.msgwar.msgWaringFunc;
	};
	inline pyfree::MsgWaring getMsgWaring()
	{
		return appConf.msgwar;
	};
protected:

private:
	BusinessDef();
	BusinessDef& operator=(const BusinessDef&) { return *this; };
	void init(); 
private:
	static BusinessDef* instance;
	//app运行参数配置集
	pyfree::AppConf appConf;
};

#endif


