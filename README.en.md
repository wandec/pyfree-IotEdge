# pyfree-IotEdge

#### Description
c++开发的一套物联网边缘服务系统，当前主要包含采集和调度两个子系统，采集子系统用于设备的供电、态势、IT资源等信息采集,支持串口、网口、蓝牙等采集接口，以及采集数据的分析、中转、级联；调度子系统通过可定制的定期、定时、轮询、条件、启停等任务策略以及可视化监控终端实现设备态势的自动化调度与运维

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
