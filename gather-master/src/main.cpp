#include "version.h"

#include <stdio.h>
#include <stdlib.h>

#include "appexitio.h"
#include "gathermgr.h"
#include "lib_acl.h"
#include "Log.h"

namespace GlobalVar {
	std::string m_app_conf_file = "appconf.xml";
	bool exitFlag = false;
	extern std::string logname;
};

#ifdef WIN32
//server conf
char SVCNAME[128] = "pyfreeGather";
char SVCDESC[256] =
"\r\n pyfree Technology Ltd \r\n "
"https://gitee.com/pyzxjfree/pyfree-IotEdge \r\n "
"email:py8105@163.com \r\n "
"pyfree-gather system service \r\n "
"Service installation success";
#endif // WIN32

#ifdef WIN32
int MyMain(int argc, char* argv[])
#else
int main(int argc, char *argv[])
#endif
{
	#ifdef WIN32
	GlobalVar::logname = std::string(SVCNAME);
	#else
	GlobalVar::logname = "pyfreeGather";	//采用服务名最好与win服务名、linux安装脚本的文件保持一致
	#endif
	if (!pyfree::LicenseCheck())
	{
		CLogger::createInstance()->Log(MsgWarn
			,"license is error, please make sure software instance is right first!");
		sleep(100);
		exit(true);
	}
	pyfree::versionLog();
	pyfree::checkArg(argc,argv);
	GatherMgr *gm_ = new GatherMgr();
	gm_->start();
	//
#ifdef WIN32
	if (!SetConsoleCtrlHandler((PHANDLER_ROUTINE)ctrlhandler, true))
	{
		CLogger::createInstance()->Log(MsgWarn, "install signal handler error!");
	}
#else
	SignalHandler * g_exit_handler = NULL;
	g_exit_handler = new SignalHandler();
	if(!g_exit_handler){
		CLogger::createInstance()->Log(MsgWarn, "install signal handler error!");
	}
#endif // WIN32

	while (!GlobalVar::exitFlag)
	{
		sleep(10);
	}
	return 0;
}
