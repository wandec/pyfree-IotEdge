#ifndef _SERIAL_G_CHANNEL_H_
#define _SERIAL_G_CHANNEL_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : serialgchannel.h
  *File Mark       : 
  *Summary         : 串口采集端接口实现
  *for serial port open-close and data-read&write
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <vector>
#include <string>

#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "gchannel.h"

#include "gsmmsg.h"
#include "dtypedef.h"
#include "datadef.h"

class SerialGChannel : public GatherChannel,public acl::thread
{
public:
	SerialGChannel(int gid_);
    ~SerialGChannel();

	
	void* run();
	/**
	 * 上行接口调用本采集信道的下控执行函数,重写父类的函数
	 * @param exetype {int} 执行类型{1,查询;2,设值}
	 * @param type {PType} 信息点类型{遥信/遥测等}
	 * @param pid {int} 信息点编号
	 * @param val {float} 值
	 * @param task_id {ulong} 任务编号
	 * @return {void } 
	 */
	void downControl(int _exetype, int _type, int _pid, float _val, unsigned long _taskID=0);
private:
	/**
	 * 本实例初始化
     * @param gid_ {int} 采集口编号
	 * @return {void } 
	 */
	void init(int gid_);
	/**
	 * 串口通信,从缓存队列中读取下控指令,进行下行通信,并等待设备端的上行通信返回
	 * 对返回报文进行实时规约分析,具体分析过程由lua脚本实现
	 * @return {void } 
	 */
	void serial_call();
	/**
	 * 从总召指令列表中读取召唤指令,添加到待下发指令队列中,并获取返回指令进行规约解析
	 * @return {void } 
	 */
	void push_total_call();
	/**
	 * 串口信道信息点态势定期上送
	 * @return {void } 
	 */
	void data_time_up();
	/**
	 * 打开串口并标记串口打开状态
	 * @return {void } 
	 */
	bool open();
	/**
	 * 真正的打开串口实现
	 * @return {void } 
	 */
	bool startSms();
	/**
	 * 关闭串口
	 * @return {void } 
	 */
	void stopSms();
	/**
	 * 向下发队列添加指令信息
     * @param text {char*} 指令信息
	 * @param _wait {int} 等待时间
	 * @param appendf {bool} 添加队列[true]未还是队列头[false]
	 * @param acsiif {bool} acsii编码支持
	 * @param _taskID {ulong} 任务编号
	 * @return {void } 
	 */
	void sendmsg(const char* text,int _wait=-1
		, bool appendf=true,bool acsiif=false, unsigned long _taskID=0);
	/**
	 * 对来自采集网口的报文进行解析
	 * @param ret {bool} 下控结果
	 * @param text {char*} 下行报文指针
	 * @param atres {char*} 上行报文指针
	 * @param exception {char*} 发送报文返回的异常信息
     * @param wf {bool} 写入标记
	 * @param controlf {bool} 下控指令还是总召指令
	 * @param _taskID {ulong} 任务编号
	 * @return {void } 
	 */
	void msgResponse(bool ret, const char *text,char *atres,char* exception,bool wf
		,bool controlf=false,unsigned long _taskID=0);
private:
	bool running;						//线程循环标识
	bool serial_open_flag;				//串口打开标识
	unsigned int serialPortStateTime;	//端口态势上送时间节点
	
	pyfree::SerialPort serialarg;		//串口定义
	gsmmsg m_ssg;						//真正的串口通信接口,采用ctb库实现
	
	int allCmdFlag;						//总召指令特情,在返回异常时用于区分特定处置
	unsigned int error_ret_count;		//下发指令响应异常次数
	static const unsigned int msg_Max_count = 20; 			//缓存指令大小限制
	static const unsigned int error_ret_max = 10;			//响应异常次数限制
	static const unsigned int error_ret_max_AllCmd = 100;	//有总召指令时响应异常次数
};

#endif
